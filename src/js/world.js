//FIXME
//Sunset is not position based

var positions;
var pathData; //contains all the latlang
var linesData;//contains an array of all lines
var stroke; //array of lines of the last X positions
var lastPosition;
var map;
var marker;
var polylines;
var moment;
var pictures;
var initialTimestamp = new Date().getTime();
var pixelToMiliseconds = 10000;
var minDate = 0;
var currentPage = 1;
var waitingForData = false;
var mapDayStyle =[
  {
  },{
  }
];

var mapNightStyle = [
  {
    "stylers": [
      { "gamma": 0.92 },
      { "saturation": -69 },
      { "lightness": -13 }
    ]
  }
];

mapSunsetStyle = [
  {
    "stylers": [
      { "saturation": -31 },
      { "lightness": -7 },
      { "gamma": 0.97 }
    ]
  }
];

var mapStyle=[
  {
    "featureType": "administrative.country",
    "stylers": [
      { "visibility": "on" },
      { "lightness": 68 }
    ]
  },{
    "featureType": "administrative.province",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "administrative.locality",
    "stylers": [
      { "visibility": "on" },
      { "gamma": 2.01 }
    ]
  },{
    "featureType": "administrative.neighborhood",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "water",
    "stylers": [
      { "hue": "#00bbff" },
      { "saturation": 59 },
      { "visibility": "simplified" },
      { "lightness": 18 },
      { "weight": 1.6 }
    ]
  },{
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "gamma": 6.67 },
      { "saturation": -59 }
    ]
  },{
    "featureType": "road",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      { "hue": "#ffa200" },
      { "gamma": 1 },
      { "saturation": -71 },
      { "visibility": "simplified" }
    ]
  },{
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [
      { "lightness": -10 }
    ]
  },{
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "simplified" },
      { "hue": "#ffc300" },
      { "saturation": -100 },
      { "gamma": 9.99 },
      { "lightness": 40 }
    ]
  },{
    "featureType": "transit",
    "stylers": [
      { "visibility": "off" }
    ]
  },{
    "featureType": "poi",
    "elementType": "labels",
    "stylers": [
      { "visibility": "off" }
    ]
  }
]

function documentReady()
{
    initMoment(); 
    google.maps.event.addDomListener(window, 'load', initMap);
}

function initMap()
{
    var mapOptions = {
        center: new google.maps.LatLng(29,41.1),
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        scrollwheel: false,

        streetViewControl: true,

        zoomControl:true,
        scaleControl: true,
        mapTypeControlOptions: {
          //mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'sexy_map'] //TODO: remove
        },
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL,
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        }
    };

  map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
  //map.mapTypes.set('sexy_map', styledMap);
  //map.setMapTypeId('sexy_map');
  google.maps.event.addListenerOnce(map, 'idle', onMapsLoaded);
}

function initMoment()
{
    moment = $('#moment');
}

function reverseArray(array)
{
    var newArray = new Array();
    for(i=array.length-1; i>=0; i--)
        newArray.push(array[i]);
    return newArray;
}

function scroll(scrollPosition)
{
    var scrollTime = scrollPosition * pixelToMiliseconds;
    var scrollMoment = new Date().getTime() - scrollTime; //CURRENT TIME
    moment.text(new Date(scrollMoment).toString());

   // console.log(scrollPosition * $(window).height() / ($(document).height()));

    if((pathData == null) || (pathData.length <= 0))
      return
    
    var index = findIndexByTime(scrollMoment);
    var interpolatedPosition = findPositionByTime(index,scrollMoment);
    
    if(index <0)
        index = 0;
    if(index >pathData.length - 1)
        index = pathData.length - 1;

    moveMarkerTo(pathData[index]);
    updateStroke(index);
    panTo(pathData[index]);
   // if(scrollTime > 1367366400000) // 01 May 2013 00:00:00 GMT
    //    return;
    checkForFetching(scrollPosition);
    checkForDayLight(scrollMoment, pathData[index]);
}

function checkForDayLight(time, position)
{
  //TODO just calculate this once per day, not on every scroll change
  var date = new Date().setTime(time);
  var times = SunCalc.getTimes(date, position.lat(), position.lng());
  //eee

  var sunriseStart = times.sunrise.getTime() ; //(top edge of the sun appears on the horizon)
  var sunriseEnd = sunriseStart + 1*60*60*1000; //  one hour later 
  var sunsetEnd = times.sunset.getTime(); // sun disappears below the horizon
  var sunsetStart = sunsetEnd - 1*60*60*1000; //  one hour earlier 
  var previousSunsetEnd = sunsetEnd - 24*60*60*1000;
  var nextSunriseStart = sunriseStart +  24*60*60*1000;
  console.log(" PrevSunsetEnd:\t"+ new Date(previousSunsetEnd) + "\n Now:\t" + new Date(time) +"\n Sunrise\t: "+ new Date(sunriseStart)+ "\n");

  if(((previousSunsetEnd < time) && (sunriseStart > time)) || ((sunsetEnd < time) && (nextSunriseStart > time)))
    setNightStyle();
  else if((sunriseStart < time) && (sunriseEnd > time))
    setSunriseStyle();
  else if((sunriseEnd < time) && (sunsetStart > time))
    setDayStyle();
  else if ((sunsetStart < time) && (sunsetEnd > time))
    setSunsetStyle();
  else
    setDayStyle();
}

function setSunriseStyle()
{
  map.setOptions({styles: mapSunsetStyle});
  console.log ("Sunrise");
}

function setSunsetStyle()
{
  map.setOptions({styles: mapSunsetStyle});
  console.log ("Sunset");
}

function setDayStyle()
{
  map.setOptions({styles: mapDayStyle});
  console.log("Day");
  //('sexy_map', styledMap);
}

function setNightStyle()
{
  map.setOptions({styles: mapNightStyle});
  console.log("Night");
  //('sexy_map', styledMap);
}

function checkForFetching(scrollPosition)
{
    if(scrollPosition > $(document).height() -2000)
    {
        if(!waitingForData)
        {
            waitingForData = true;
            currentPage ++;
            loadFlickrData();
        }
    }
       
}

function panTo(latlng)
{
  map.panToWithOffset(latlng, -150, 0);
}

google.maps.Map.prototype.panToWithOffset = function(latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
    }; 
    ov.draw = function() {}; 
    ov.setMap(this); 
};

function moveMarkerTo(position)
{
  marker.setPosition(position);
}

function zoomIn()
{
  if(map.getZoom() == 11)
  {
    panTo(new google.maps.LatLng(lastPosition.lat,lastPosition.lon));
    return;
  }
  map.setZoom(map.getZoom()+1);
  setTimeout(zoomIn, 100);
}

function initPath()
{
  pathData = new Array();
  for(i = 0; i<positions.length; i++)
    pathData.push( new google.maps.LatLng(positions[i].lat,positions[i].lon));
}

function initLinesData()
{
    linesData = new Array(); //Array of arrays of 2 points polylines

    for(i = 0; i< pathData.length; i++)
    {
      var line =  new Array(pathData[i], pathData[i+1]);
      linesData.push(line);
    }
}

function initStroke()
{
   stroke = new Array();

    for(i = 0; i< 50; i++)
    {
      var line = new google.maps.Polyline({
        path : linesData[i],
        strokeColor: '#FF3366',
        strokeOpacity: 0,
        strokeWeight: 2
      });

      line.setMap(map);
      stroke.push(line);
    } 

    styleStroke();
}

function styleStroke()
{
  var opacity = 1;
  var thikness = 2;

  for(i = 0 ; i<stroke.length ; i++)
    {
        opacity -= 1/stroke.length;
        if(opacity <0)
            opacity = 0;

        thikness -= 2/stroke.length;
        if(thikness <0)
            thikness = 0;

        stroke[i].setOptions(
        {
            strokeOpacity: opacity,
            strokeWeight: thikness
        });
    } 
}

function updateStroke(index)
{  
    var tailLength = stroke.length;
    
    for(i = 0 ; i<tailLength ; i++)
    {

        stroke[i].setOptions(
        {
            path : linesData[index + i]
        });
    } 
}

function findIndexByTime(time)
{
    for(i = 0 ; i<positions.length ; i++)
    {
      if(positions[i].t * 1000 < time)
        return i; 
    }
    return 0;
}

function findPositionByTime(index, moment)
{
    var deltaTime = positions[index].t - positions[index+1].t;
    var deltaLat = positions[index].lat - positions[index+1].lat;
    var deltaLng = positions[index].lng - positions[index+1].lng;

    currentTime = moment - positions[index].t;

    var currentLat = positions[index].lat + currentTime * deltaLat / deltaTime;
    var currentLng = positions[index].lng + currentTime * deltaLng / deltaTime;

    return new google.maps.LatLng(currentLat, currentLng); 
}

function onMapsLoaded()
{
  initFullImage();
  loadPositions();
  loadFlickrData();
  $("#more-data").hide();
}


function loadPositions()
{
  $.ajax({
    dataType: 'jsonp',
    success: onLocationsLoaded,
    jsonpCallback:'jsonp_callback',
    contentType: "application/json",
    crossDomain: true,
    url: 'http://xavivives.com/world/request.py',
    error: function() { console.log("Error on loadPositions")}
  });
}

function onLocationsLoaded(data)
{
    positions = reverseArray(data);
    lastPosition = data[data.length-1];
    map.panTo(new google.maps.LatLng(lastPosition.lat,lastPosition.lon));
    //map.setZoom(12);
    setTimeout(zoomIn, 300);

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lastPosition.lat,lastPosition.lon),
        map: map,
        title:"Xavi"
    });

    initPath();
    initLinesData();
    initStroke();
    initLastUpdate();
    $("#more-data").show();
}

function initLastUpdate()
{
  var daysSinceUpdate = Math.round((new Date().getTime() -  positions[0].t*1000)/1000/60/60/24);
  var hoursSinceUpdate = Math.round((new Date().getTime() -  positions[0].t*1000)/1000/60/60);
  var minutesSinceUpdate = Math.round((new Date().getTime() -  positions[0].t*1000)/1000/60);
  var secondsSinceUpdate = Math.round((new Date().getTime() -  positions[0].t*1000)/1000);

  if(daysSinceUpdate>0)
    $('#last-update').text("Last update was "+daysSinceUpdate+" days ago.");
  else
    if(hoursSinceUpdate>0)
      $('#last-update').text("Last update was "+hoursSinceUpdate+" hours ago.");
    else
      if(minutesSinceUpdate>0)
        $('#last-update').text("Last update was "+minutesSinceUpdate+" minutes ago.");
      else
        $('#last-update').text("Last update was "+secondsSinceUpdate+" seconds ago.");

}

function draw()
{
  var lines = new google.maps.Polyline({
    path: pathData,
    strokeColor: '#FF0000',
    strokeOpacity: 0.5,
    strokeWeight: 2
  });

  lines.setMap(map);
}

function loadFlickrData()
{
  $.ajax({
    dataType: 'json',
    success: onFlickrDataLoaded,
    jsonpCallback:'jsonp_callback',
    crossDomain: true,
    url : getFlickrRequestUrl(currentPage),
    //url: 'http://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=20129b3b3e965748c302be39bba46fc7&photoset_id=72157634032133654&extras=date_taken%2C+url_o%2C+url_m&per_page=50&page=0&format=json&nojsoncallback=1',
    error: function() { console.log("Error on loading Flickr data")}
  });
}

function getFlickrRequestUrl(page)
{
    var days = 5;
    minDate = Math.round(initialTimestamp/1000) -  60 * 60 * 24 * days * page; //in seconds
    var maxDate = Math.round(initialTimestamp/1000) -  60 * 60 * 24 * days * (page -1);
    return "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=20129b3b3e965748c302be39bba46fc7&user_id=49345494%40N07&min_taken_date="+minDate+"&max_taken_date="+maxDate+"&sort=date-taken-desc&extras=description%2C+date_taken%2C+url_o%2C+url_m&format=json&nojsoncallback=1";
}

function onFlickrDataLoaded(data)
{
    pictures = data.photos.photo;

    for(i=0; i<pictures.length; i++)
       createImagePost(pictures[i]);

    setMoreDataLabel();
    waitingForData = false;
}

function setMoreDataLabel()
{
    $("#more-data").show();
    var position = getPositionByTimeStamp(minDate*1000);
    if(position < prevImgBottom)
        position = prevImgBottom + 600;
    $("#feed-footer").css("top", position+"px");
}

function addTimeZone(date, hours)
{
    var miliseconds = hours * 60 * 60 * 1000;
    var newDate = new Date(date.getTime() + miliseconds);
    return newDate;
}

function getPositionByTimeStamp(timeStamp)
{
    var timeZoneOffset = new Date().getTimezoneOffset()*60*1000;
    var localDate = new Date (new Date().getTime() + timeZoneOffset);
    var position =Math.round((localDate.getTime() - timeStamp) / pixelToMiliseconds);
    return position
}

var prevImgBottom = 0;

function createImagePost(imgData)
{
    var imgTimeStamp = Date.parse(imgData.datetaken).getTime(); //GMT
    //var timeZoneOffset = new Date().getTimezoneOffset()*60*1000;
    //var localDate = new Date (new Date().getTime() + timeZoneOffset);
    //var position =Math.round((localDate.getTime() - date.getTime()) / pixelToMiliseconds);
    var position = getPositionByTimeStamp(imgTimeStamp);
    if(position < prevImgBottom)
        position = prevImgBottom;
    
    //var img = '<a href="'+imgData.url_o+'"><img src="'+imgData.url_m+'" alt="'+imgData.title+'" ></a>';
    var img = '<div><img src="'+imgData.url_m+'" alt="'+imgData.title+'" ></div>';
    var description = "";
    if(imgData.description._content.indexOf("OLYMPUS")!= -1)
    {
      description = " ";
    }
    else
    {
      description =  '<div style = " position: absolute; width:'+(imgData.width_m - 30)+'px; padding: 15px; background-color: rgb(253, 253, 253); opacity: 0.7; "> '+imgData.description._content+'</div>';
    }

    var content = '<div class="post post-img" id="post'+imgData.id+'" style = "width:'+imgData.width_m+'px; height:'+imgData.height_m+'px; top:'+position+'px;">'+description+img+'</div>';

    $('#post-feed').append(content);

    $("#post"+imgData.id).click(function(e) {
       e.preventDefault();
       window.open(imgData.url_o);
      // $("#fullimage").backstretch(imgData.url_o);
       //$("#fullimage").show();
     });

    prevImgBottom = position + parseInt(imgData.height_m) + 20;
    //return content; 
}

function getSolarAngle(lat,lng,timeStamp)
{
    //var meanObliqEcliptic = 23+(26+((21,448-G2*(46,815+G2*(0,00059-G2*0,001813))))/60)/60 
    //var ObliqCorr = meanObliqEcliptic+0,00256*Math.cos(toRad(125,04-1934,136*G2))
  //var sunDeclination = toDegree(Math.asin(Math.sin(toRad(R2))*Math.sin(toRad(sunAppLong))))
  //var hourAngle = 
  //=toDegree(Math.acos(Math.sin(toRad(lat))*Math.sin(toRad(sunDeclination))+Math.cos(toRad(lat))*Math.cos(toRad(sunDeclination))*Math.cos(toRad(hourAngle))))
  
/*
  var F2 = toJulian(timeStamp);
  var G2 =(F2-2451545)/36525;
  var J2 =357,52911+G2*(35999,05029 - 0,0001537*G2);
  var I2 = ((280,46646+G2*(36000,76983 + G2*0,0003032)) % 360;
  var L2 = Math.sin(toRad(J2))*(1,914602-G2*(0,004817+0,000014*G2))+Math.sin(toRad(2*J2))*(0,019993-0,000101*G2)+Math.sin(toRad(3*J2))*0,000289;
  var Q2 = 23+(26+((21,448-G2*(46,815+G2*(0,00059-G2*0,001813))))/60)/60
  var M2 = I2 + L2;
  var R2 = Q2+0,00256*Math.cos(toRad(125,04-1934,136*G2));
  var P2 = M2-0,00569-0,00478*Math.sin(toRad(125,04-1934,136*G2));
  var AB2 = =(E2*1440+V2+4*lng) % 1440
  if()
  var AC2 = SI(AB2/4<0;AB2/4+180;AB2/4-180);
  var T2  = toDegree(Math.sin(Math.sin(toRad(R2))*Math.sin(toRad(P2))));
  var solarZenithAngle = toDegree(Math.cos(Math.sin(toRad(lat))*Math.sin(toRad(T2))+Math.cos(toRad(lat))*Math.cos(toRad(T2))*Math.cos(toRad(AC2))));
*/

var SunCalc = require('./suncalc'),
    assert = require('assert');

}

function toJulian(timeStamp)
{
    return ( timeStamp / 1000 / 86400.0 ) + 2440587.5;
}

function toRad(degree)
{
  return degree * 2 * Math.PI / 360;
}

function toDegree(radiant)
{
    return radiant * 360 / 2 * Math.PI;
}

function initFullImage()
{
  $("#fullimage").hide();
  $("#fullimage").click(function(e) {
    e.preventDefault();
    $("#fullimage").hide();
  });
}

$(window).scroll(function() {
    var position = $(document).scrollTop();
     scroll(position); 
 });


