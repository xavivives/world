from mod_python import apache
    

import oauth2, time, urllib, urllib2, json
import sys

#print 'Number of arguments:', len(sys.argv), 'arguments.'
#print 'Argument List:', str(sys.argv)

content = ""


now = time.time()
startTime = now - 24*60*60
endTime = now
days = 70
length = 2000

if len(sys.argv) >= 2:
    days = int(sys.argv[1])
if len(sys.argv) >= 3:
    startTime = sys.argv[2]
if len(sys.argv) >= 4:
    length = sys.argv[3]

startTime = endTime - days*24*60*60


#print 'arguments', startTime, startTime, length

ACCESS = "HFMS5XOMCLLLZK4FC25CTTCLOIXYZ4YVSQMFFXS2YN3XG57LTAPA"
SECRET = "TYOZ3BEPB7XEXMB8BYSEB5CP24TZWGW04EU0AZWKM2J1NH86SF6Y1JGW3QKEU8I7"
URL = "https://openpaths.cc/api/1"

def addContent(s):
    global content
    content+=s


def build_auth_header(url, method):
    params = {                                            
        'oauth_version': "1.0",
        'oauth_nonce': oauth2.generate_nonce(),
        'oauth_timestamp': int(time.time()),
    }
    consumer = oauth2.Consumer(key=ACCESS, secret=SECRET)
    params['oauth_consumer_key'] = consumer.key 
    request = oauth2.Request(method=method, url=url, parameters=params)    
    signature_method = oauth2.SignatureMethod_HMAC_SHA1()
    request.sign_request(signature_method, consumer, None)
    return request.to_header()

# GET data (last 24 hours)
def getData():
    #params = {'start_time': startTime, 'end_time': endTime}    # get the last 24 hours
    params = {'num_points':2000}
    query = "%s?%s" % (URL, urllib.urlencode(params))
    try:
        request = urllib2.Request(query)
        request.headers = build_auth_header(URL, 'GET')
        connection = urllib2.urlopen(request)
        data = json.loads(''.join(connection.readlines()))
        #for some reason it duplicates the whole content. Reseting it just in case
        global content
        content = ""
        addContent('jsonp_callback(')
        addContent(json.dumps(data, indent=4))
        addContent(')')
    except urllib2.HTTPError as e:
        addContent(e.read())

def handler(req):
    getData()
    global content
    req.content_type = "text/plain"
    req.write(content)
    return apache.OK  